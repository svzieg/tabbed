fork of https://tools.suckless.org/tabbed/

## Changes
added some patches/settings for tabs and vimb usage







# tabbed - generic tabbed interface
tabbed is a simple tabbed X window container.

## Requirements
In order to build tabbed you need the Xlib header files.

## Installation
Edit config.mk to match your local setup (tabbed is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install tabbed
(if necessary as root):

```bash
make clean install
```

## Running tabbed
See the man page for details.

